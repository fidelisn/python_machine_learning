
'''
Overview:
     
kNN: k Nearest Neighbors algorithm implementation

    Input:      inX: vector to compare to existing dataset (1xN)
    dataSet: size m data set of known vectors (NxM)
    labels: data set labels (1xM vector)
    k: number of neighbors to use for comparison (should be an odd number)
    Output:     the most popular class label
    
@author: Fidelis Njikem adopted from  pbharrin
'''

###### import depencies ###########
from numpy import *
import operator
from os import listdir
import matplotlib.pyplot as plt
####################################

########################################
'''
def knn_classify0():
    dataSet = array([[1.0,1.0],[2,1],[2,0.1],[2,0.1],[1,0.1],[1,0.1]]) #training set
    labels = ['A','E','B','B','C','A']  #labels for training set
    inX = [1,0] #testing set. dataset to be predicted
    num_labels = len(labels) #get number of labels
    dataSetSize = dataSet.shape[0] # get size of training dataset

    #############################################
    #calculation of euclidean distance between query data instance  and all other pre-existing dataset (data instances)

    diffMat = tile(inX, (dataSetSize,1)) - dataSet # tile repeats the unknown dataset the number total data instant times and the minus dataSet subtracts the replicate from  each data instance
    sqDiffMat= diffMat**2 #square all the elements of the euc distance matrix
    sqDistances = sqDiffMat.sum(axis=1)#add all elements of the euc dist matrix
    distances = sqDistances**0.5 #sqre root of every element gives the euc dist
    sortedDistIndicies = distances.argsort() # sort the distances by thier indices
    classCount={} # create empty dictionary

    for i in range(3):
        voteIlabel = labels[sortedDistIndicies[i]]
        classCount[voteIlabel] = classCount.get(voteIlabel,0) + 1
    sortedClassCount = sorted(classCount.iteritems(), key=operator.itemgetter(1), reverse=True)
    pred_class =sortedClassCount[0][0]

    print "predicted class for data instance: ", inX, "is :" , pred_class

    print classCount

    print sortedClassCount

##################################################


###########################################################
## plot data so as to visualise it ###

def plot_knn_classify0():
    x= [1.0,1.0,3.0,2]  #vectors of feature x
    y =[1.0,2.0,2.0,1 ] # vectors of feature y
    labels = ['A','B','C','?'] # array holding labels
    plt.scatter(x,y,label='data instances')          # plot data

    for i, txt in enumerate(labels):
        plt.annotate(txt, (x[i],y[i]))
                    
    plt.title('KNN data')
    plt.xlabel('X-axis')
    plt.ylabel('Y-axis')
    plt.legend()
    plt.show()
#################################################################
'''

'''
##################################################################################
# function to reads data from file, puts it in a matrix
# function also read labels from same file and puts them in thier own matrix
##################################################################################

def file2matrix():
    #path to file
    filename ='/Users/fidelis/Desktop/machinelearninginaction-master/Ch02/datingTestSet.txt';
    fr = open(filename);  #open file and pass data to variable fr
    arrayOLines = fr.readlines(); # read all of the data and pass it to arrayOlines variable
    numberOfLines = len(arrayOLines);  #get the number of lines in the file
    returnMat = zeros((numberOfLines,3))   #prepare matrix to return of 1000rows, 3 cols
    classLabelVector = []  #create vector to hold labels
    love_dictionary={'largeDoses':3, 'smallDoses':2, 'didntLike':1} # dictionary that gives number value to type of like
    index = 0  #create and initialize control var for loop
    for line in arrayOLines:     #loop through each line of the text file
        line = line.strip()   # strip of return line character
        listFromLine = line.split('\t')  # split line into list of elements
        #deleimited by '\t'
        
        returnMat[index,:] = listFromLine[0:3] #take first three elements
        #shove into each row of matrix
          
          #use pythons negative indexing to get last item from the list(the label)
        if(listFromLine[-1].isdigit()): # if last item on list is a digit
          classLabelVector.append(int(listFromLine[-1])) #add to class label vector
        else:
          classLabelVector.append(love_dictionary.get(listFromLine[-1])) # get numeric value for
        index += 1 # increment index
    return returnMat,classLabelVector
########################################################################################
'''


############################################################
## function to normalise values
#normalisation reduces the impact of large numbers that dont actually affect the classes
# the function below normalises the data between 0 and 1

def autoNorm():
    dataSet = array([[1.0,1.0],[2,1],[2,0.1],[2,0.1],[1,0.1],[1,0.1]]) #training set
    minVals = dataSet.min(0)
    maxVals = dataSet.max(0)
    ranges = maxVals - minVals
    normDataSet = zeros(shape(dataSet))
    m = dataSet.shape[0]
    normDataSet = dataSet - tile(minVals, (m,1))
    normDataSet = normDataSet/tile(ranges, (m,1))   #element wise divide
    print ranges;
    return normDataSet, ranges, minVals




'''
#FUNCTION CALLS
#########################################
knn_classify0();
plot_knn_classify0();
dating_datamatrix, datinglabels = file2matrix()
fig = plt.figure();
ax = fig.add_subplot(111);
ax.scatter(dating_datamatrix[:,0], dating_datamatrix[:,1], 15.0 * array(datinglabels), 15.0*array(datinglabels) )
plt.show();
'''

autoNorm();


