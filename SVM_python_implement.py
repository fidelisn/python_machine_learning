
#!/usr/bin/python
# python implementation of SVM

from sklearn import svm
from sklearn import datasets
import matplotlib.pyplot as plt
from numpy import *
import glob, os

import pandas as pd

#read file
file_path = '/Users/fidelis/Desktop/5C_output/Galaxy6-[Convert_on_data_5].tabular'; # get excel file path that contains data

index =0;
cols = 20;
rows =  1521;
move = 39;
init =0;

dataset_counter = 0;

counter = 0;

dataset = zeros(shape=(cols,rows)) #create an array containing features
directory= "/Users/fidelis/Desktop/5C_output/IF_files_microarray" # assign directory name to string

print dataset.shape


data = {'year': [2010, 2011, 2012, 2011, 2012, 2010, 2011, 2012],
    'team': ['Bears', 'Bears', 'Bears', 'Packers', 'Packers', 'Lions', 'Lions', 'Lions'],
        'wins': [11, 8, 10, 15, 11, 6, 10, 4],
        'losses': [5, 8, 6, 1, 5, 10, 6, 12]}
football = pd.DataFrame(data, columns=['year', 'team', 'wins', 'losses'])
print football


for file in os.listdir(directory): #loop in directory containing IF files
    if file.endswith(".txt"):      # get text file

        print "\n\n\n!!!!! start processing file: ",(file)
        IF_file = open(directory+'/'+file, 'rU'); # open interaction frequency file1 file that contains data
        all_data = IF_file.readlines(); # read all lines of the file
        all_data = all_data[1:39]; # take a slice and remove first row


        for line in all_data:     #loop through each line of the text file
                line = line.strip();   # strip of return line character
                listFromLine = line.split('\t');  #split line into list of elements
                #deleimited by '\t'
                listFromLine = listFromLine[1:len(listFromLine)];
                dataset[dataset_counter,init:move] = listFromLine;
                #break;
                init +=39;
                move += 39;
                index += 1;
        dataset_counter += 1;
        print "\n\n\n ------- END processing file: ",(file)
#break;

print len(dataset[0,])
print dataset[0,0:10]



#IF_file = open(file_path, 'rU'); # open interaction frequency file1 file that contains data
#all_data = IF_file.readlines();
#all_data = all_data[1:53];
#numlines = len(all_data);




#print dataset[0,60:70]
#cem_dat = dataset[0]
#print cem_dat[-30:]
#print len(dataset[0,])





'''
labels = {'CEM':'MLLwt','HSB2':'MLL','THP-1':'MLLwt'} #create labels dictionary containing cell lines as key and MLL state as element
print "CEM class is  : " , labels['CEM']
'''




'''
digits = datasets.load_digits()
#print digits.data
print digits.target
clf = svm.SVC(gamma=0.001, C=100)  # call SVM and assign it to variable
train_dat,labels = digits.data[:-10], digits.target[:-10] # all but the last ten data set as training  data for SVM
# train the SVM
print "SVM is bieng trained";
clf.fit(train_dat,labels)
#test the SVM
print(clf.predict(digits.data[-5])) # predict the 5th from the last element
plt.imshow(digits.images[-5], cmap=plt.cm.gray_r, interpolation='nearest')
plt.show()
'''


