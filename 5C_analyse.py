#!/usr/bin/python


### OVERVIEW #####

###################

#import dependencies
import xlrd  #import package use to read excel file

#read file
file_path = '/Users/fidelis/Desktop/5C_output/HOXA_IF.xlsx'; # get excel file path that contains data
write_file ='/Users/fidelis/Desktop/5C_output/arr_file.txt'; #get file path for .arr file
file = open(file_path, 'rU');  #open excel file that contains data
arrfile = open(write_file, 'w');  #open arr file for writing
data = xlrd.open_workbook(file_path);

print "Script has been has been executed and the output could be found at :",write_file

### dataset overview #####
with  open(write_file,'w') as OUTPUT:
    OUTPUT.write("\n%OVERVIEW: the data set is composed of IF data of 5C from leukemia cell lines% \n ")
    OUTPUT.write("\n%number of data sets are:"+ str(data.nsheets) + " %\n")#print number of sheets
    OUTPUT.write("\n%The dataset cell lines are: "+ str(data.sheet_names())+ "%\n\n" ) # print sheet names'''
#### end dataset overview ######

label = 'MLLwt';


##### attributes section of arr file ####
nb4_data = data.sheet_by_index(0) #get first worksheet (first data set)
first_row= nb4_data.row_values(0)
first_col= nb4_data.col_values(0)
i =1;

with open(write_file,"a") as OUTPUT:
    for i in range(len(first_row)):
      for val in range(len(first_row)):
          row_one = first_row[i]
          col_one = first_col[val]
         #if(!is_empty(row_one[i]) or !is_empty(col_one[val])):
         #if(not row_one[l] or not col_one[l]):
         #if (len(row_one[i]) != 0 ):
          if(row_one[0:2]=="GD"):
            row_one = row_one[0:5]
          if(row_one[0:3]== "Hox"):
            row_one = row_one[0:7]
          if(col_one[0:2]== "GD"):
            col_one = col_one[0:5]
          if(col_one[0:3]== "Hox"):
            col_one = col_one[0:7]

          if(row_one!="" and col_one!=""):
            #print "@attribute", row_one+"|"+col_one , "real"
            OUTPUT.write(" ".join(["@attribute",row_one+"|"+col_one,"real"])+"\n")

    #print "\n\n------------- end of ", first_row[i], "\n\n\n"

    #print "@attribute  MLL state {MLLwt, MLLf} \n"
    OUTPUT.write("@attribute  MLL state {MLLwt, MLLf} \n")

    ###### end of attributes section of arr file ######

    ##### data section of arr file ##########

    #print "@data\n"
    OUTPUT.write("@data\n")
    j = 1;
    indx = 1;
    attribute_values=[]
    for j in range(len(first_row)):
        #print "\n\n-------------!!!!! begining of: ", first_row[j], "\n\n"
     for indx in range(len(first_row)):
         #print nb4_data.cell(indx,j)
          value = nb4_data.cell(indx,j)
          value  = str(value)
          if value.startswith("n"):
                #print value[7:],",",
                attribute_values.append(value[7:])
    #print "\n\n------------- !!!!!! end of: ", first_row[j], "\n\n\n"
    j += 1;
    ##### end of data section ################################

    #print label;
    OUTPUT.write(",".join(attribute_values+[label])+"\n")